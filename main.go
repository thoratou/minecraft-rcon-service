package main

import (
	"github.com/astaxie/beego"
	_ "gitlab.com/thoratou/minecraft-rcon-service/routers"
)

func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}

	beego.Run()
}
