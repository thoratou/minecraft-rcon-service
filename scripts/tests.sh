#!/bin/bash

export COGNITO_TOOL_CACHE_FOLDER=/go/src/gitlab.com/thoratou/minecraft-rcon-service/cache

TOKEN="$(cognito-tool get-token)"

echo "$TOKEN"

#no token
#curl -H "Accept: application/json" -ik -w"\n" -XPOST https://172.17.0.1:9081/v1/banner -d '{"to":"thoratou"}'

#ok test
#curl -H "Accept: application/json" -H "Authorization: Bearer $TOKEN" -ik -w"\n" -XPOST https://172.17.0.1:9081/v1/banner -d '{"to":"thoratou"}'


#no token
#curl -H "Accept: application/json" -ik -w"\n" -XGET https://172.17.0.1:9081/v1/whitelist

#ok test
curl -H "Accept: application/json" -H "Authorization: Bearer $TOKEN" -ik -w"\n" -XGET https://172.17.0.1:9081/v1/whitelist

#ok test
curl -H "Accept: application/json" -H "Authorization: Bearer $TOKEN" -ik -w"\n" -XPOST https://172.17.0.1:9081/v1/deop -d '{"player":"thoratou"}'
curl -H "Accept: application/json" -H "Authorization: Bearer $TOKEN" -ik -w"\n" -XPOST https://172.17.0.1:9081/v1/op -d '{"player":"thoratou"}'
