GO_SERVER_ROOT = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

.PHONY: all
all: build run

.PHONY: build
build:
	@printf "\033[32m Local build minecraft-rcon-service\n\033[0m"
	@docker build -t minecraft-rcon-service-local -f local/Dockerfile .

.PHONY: run
run:
	@printf "\033[32m Run local minecraft-rcon-service\n\033[0m"
	@docker run -it --rm \
		--name minecraft-rcon-service-local \
		--env-file=.env \
		-p 9081:8080 \
		-v $(GO_SERVER_ROOT):/go/src/gitlab.com/thoratou/minecraft-rcon-service \
		-e RCON_HOST=172.17.0.1 \
		-e RCON_PORT=27015 \
		-e RCON_PASSWORD=tuaslrcon \
		minecraft-rcon-service-local

.PHONY: bash
bash:
	@printf "\033[32m Open shell on local minecraft-rcon-service\n\033[0m"
	@docker exec -it minecraft-rcon-service-local sh


.PHONY: build-prod
build-prod:
	@printf "\033[32m Build minecraft-rcon-service\n\033[0m"
	@docker build -t minecraft-rcon-service .

.PHONY: run-prod
run-prod: clean-router-cache
	@printf "\033[32m Run minecraft-rcon-service\n\033[0m"
	@docker run --rm \
		-p 8081:8080 \
		-e RCON_HOST=172.17.0.1 \
		-e RCON_PORT=27015 \
		-e RCON_PASSWORD=tuaslrcon \
		minecraft-rcon-service

clean-router-cache:
	@printf "\033[32m Clean beego cache\n\033[0m"
	@rm -f routers/commentsRouter_controllers.go || true
	@rm -f lastupdate.tmp || true
