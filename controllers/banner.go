package controllers

import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"strconv"

	"gitlab.com/thoratou/minecraft-rcon-service/rconutils"

	"github.com/astaxie/beego"
	"gitlab.com/thoratou/minecraft-rcon-service/models"
	"gitlab.com/thoratou/minecraft-rcon-service/queryutils"
)

// BannerController controller to give a banner to a minecraft user with patterns based on his name
type BannerController struct {
	beego.Controller
}

// Banner entry for banner to sent to player on the minecraft server
// @Title Banner
// @Description banner to sent to player on the minecraft server
// @Success 200 {object} models.BannerResponse
// @router / [post]
func (c *BannerController) Banner() {
	var bannerQuery models.BannerQuery
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &bannerQuery); err != nil {
		queryutils.ReplyError(&c.Controller, queryutils.BADREQUEST, "cannot unmarshall banner request body", err)
		return
	}

	if err := c.validateQuery(&bannerQuery); err != nil {
		queryutils.ReplyError(&c.Controller, queryutils.BADREQUEST, "missing data on message request body", err)
		return
	}

	banner := c.generateBannerFromName(bannerQuery.To)

	response, err := rconutils.Command("give", bannerQuery.To, c.generateBannerCommandParam(banner))
	if err != nil {
		queryutils.ReplyError(&c.Controller, queryutils.SERVERINTERNALERROR, "issue while sending message command", err)
		return
	}

	bannerReply := &models.BannerReply{
		To:      bannerQuery.To,
		Message: response,
		Banner:  banner,
	}

	queryutils.ReplyWithStruct(&c.Controller, queryutils.OK, bannerReply)
}

func (c *BannerController) validateQuery(bannerQuery *models.BannerQuery) error {
	if bannerQuery.To == "" {
		return queryutils.NewError("missing target player")
	}
	return nil
}

func (c *BannerController) generateBannerFromName(player string) *models.Banner {
	//md5 algorithm returns  bytes array
	playerMd5 := md5.Sum([]byte(player))

	//color from first byte
	byte0 := playerMd5[0]
	base := (int)(byte0) % 16
	baseStr := models.BaseList[base]
	// spare : (int)(byte0) / 16

	byte1 := playerMd5[1]
	color1 := (int)(byte1) / 16
	color2 := (int)(byte1) % 16

	byte2 := playerMd5[2]
	color3 := (int)(byte2) / 16
	color4 := (int)(byte2) % 16

	byte3 := playerMd5[3]
	color5 := (int)(byte3) / 16
	color6 := (int)(byte3) % 16

	byte4 := playerMd5[4]
	byte5 := playerMd5[5]
	short45 := (int)(byte4)*256 + (int)(byte5)
	pattern1 := models.PatternList[short45%39]

	byte6 := playerMd5[6]
	byte7 := playerMd5[7]
	short67 := (int)(byte6)*256 + (int)(byte7)
	pattern2 := models.PatternList[short67%39]

	byte8 := playerMd5[8]
	byte9 := playerMd5[9]
	short89 := (int)(byte8)*256 + (int)(byte9)
	pattern3 := models.PatternList[short89%39]

	byteA := playerMd5[10]
	byteB := playerMd5[11]
	shortAB := (int)(byteA)*256 + (int)(byteB)
	pattern4 := models.PatternList[shortAB%39]

	byteC := playerMd5[12]
	byteD := playerMd5[13]
	shortCD := (int)(byteC)*256 + (int)(byteD)
	pattern5 := models.PatternList[shortCD%39]

	byteE := playerMd5[14]
	byteF := playerMd5[15]
	shortEF := (int)(byteE)*256 + (int)(byteF)
	pattern6 := models.PatternList[shortEF%39]

	return &models.Banner{
		Base: baseStr,
		Patterns: []models.Pattern{
			{
				Pattern: pattern1,
				Color:   color1,
			},
			{
				Pattern: pattern2,
				Color:   color2,
			},
			{
				Pattern: pattern3,
				Color:   color3,
			},
			{
				Pattern: pattern4,
				Color:   color4,
			},
			{
				Pattern: pattern5,
				Color:   color5,
			},
			{
				Pattern: pattern6,
				Color:   color6,
			},
		},
	}

}

func (c *BannerController) generateBannerCommandParam(banner *models.Banner) string {
	//example: minecraft:black_banner{BlockEntityTag:{Patterns:[{Color:1,Pattern:"cr"},{Color:15,Pattern:"ss"},{Color:1,Pattern:"flo"},{Color:15,Pattern:"mr"},{Color:1,Pattern:"mc"},{Color:1,Pattern:"sc"}]}}
	var buffer bytes.Buffer
	buffer.WriteString("minecraft:")
	buffer.WriteString(banner.Base)
	buffer.WriteString("{BlockEntityTag:{Patterns:[{Color:")
	buffer.WriteString(strconv.Itoa(banner.Patterns[0].Color))
	buffer.WriteString(",Pattern:\"")
	buffer.WriteString(banner.Patterns[0].Pattern)
	buffer.WriteString("\"},{Color:")
	buffer.WriteString(strconv.Itoa(banner.Patterns[1].Color))
	buffer.WriteString(",Pattern:\"")
	buffer.WriteString(banner.Patterns[1].Pattern)
	buffer.WriteString("\"},{Color:")
	buffer.WriteString(strconv.Itoa(banner.Patterns[2].Color))
	buffer.WriteString(",Pattern:\"")
	buffer.WriteString(banner.Patterns[2].Pattern)
	buffer.WriteString("\"},{Color:")
	buffer.WriteString(strconv.Itoa(banner.Patterns[3].Color))
	buffer.WriteString(",Pattern:\"")
	buffer.WriteString(banner.Patterns[3].Pattern)
	buffer.WriteString("\"},{Color:")
	buffer.WriteString(strconv.Itoa(banner.Patterns[4].Color))
	buffer.WriteString(",Pattern:\"")
	buffer.WriteString(banner.Patterns[4].Pattern)
	buffer.WriteString("\"},{Color:")
	buffer.WriteString(strconv.Itoa(banner.Patterns[5].Color))
	buffer.WriteString(",Pattern:\"")
	buffer.WriteString(banner.Patterns[5].Pattern)
	buffer.WriteString("\"}]}}")
	return buffer.String()
}
