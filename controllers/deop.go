package controllers

import (
	"encoding/json"

	"github.com/astaxie/beego"
	"gitlab.com/thoratou/minecraft-rcon-service/models"
	"gitlab.com/thoratou/minecraft-rcon-service/queryutils"
	"gitlab.com/thoratou/minecraft-rcon-service/rconutils"
)

// DeopController controller to remove minecraft players from ops
type DeopController struct {
	beego.Controller
}

// Deop entry to remove minecraft players from ops
// @Title Deop
// @Description entry to remove minecraft players from ops
// @Success 200 {object} models.APIResponse
// @router / [post]
func (c *DeopController) Deop() {
	var query models.DeopQuery
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &query); err != nil {
		queryutils.ReplyError(
			&c.Controller,
			queryutils.BADREQUEST,
			"cannot unmarshall deop request body", err)
		return
	}

	if err := c.validateQuery(&query); err != nil {
		queryutils.ReplyError(
			&c.Controller,
			queryutils.BADREQUEST,
			"missing data on deop request body", err)
		return
	}

	response, err := rconutils.Command("deop", query.Player)
	if err != nil {
		queryutils.ReplyError(
			&c.Controller,
			queryutils.SERVERINTERNALERROR,
			"issue while sending deop command", err)
		return
	}

	queryutils.ReplyOK(&c.Controller, queryutils.OK, response)
}

func (c *DeopController) validateQuery(query *models.DeopQuery) error {
	if query.Player == "" {
		return queryutils.NewError("missing player")
	}
	return nil
}
