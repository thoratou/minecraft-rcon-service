package controllers

import (
	"encoding/json"

	"github.com/astaxie/beego"
	"gitlab.com/thoratou/minecraft-rcon-service/models"
	"gitlab.com/thoratou/minecraft-rcon-service/queryutils"
	"gitlab.com/thoratou/minecraft-rcon-service/rconutils"
)

// WhitelistAddController controller to add minecraft players to whitelist
type WhitelistAddController struct {
	beego.Controller
}

// Add entry to add minecraft players to whitelist
// @Title Add
// @Description entry to add minecraft players to whitelist
// @Success 200 {object} models.APIResponse
// @router / [post]
func (c *WhitelistAddController) Add() {
	var query models.WhitelistAddQuery
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &query); err != nil {
		queryutils.ReplyError(&c.Controller, queryutils.BADREQUEST, "cannot unmarshall whitelist request body", err)
		return
	}

	if err := c.validateQuery(&query); err != nil {
		queryutils.ReplyError(&c.Controller, queryutils.BADREQUEST, "missing data on whitelist request body", err)
		return
	}

	response, err := rconutils.Command("whitelist", "add", query.Player)
	if err != nil {
		queryutils.ReplyError(&c.Controller, queryutils.SERVERINTERNALERROR, "issue while sending whitelist command", err)
		return
	}

	queryutils.ReplyOK(&c.Controller, queryutils.OK, response)
}

func (c *WhitelistAddController) validateQuery(query *models.WhitelistAddQuery) error {
	if query.Player == "" {
		return queryutils.NewError("missing player")
	}
	return nil
}
