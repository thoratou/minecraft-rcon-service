package controllers

import (
	"regexp"

	"github.com/astaxie/beego"
	"gitlab.com/thoratou/minecraft-rcon-service/models"
	"gitlab.com/thoratou/minecraft-rcon-service/queryutils"
	"gitlab.com/thoratou/minecraft-rcon-service/rconutils"
)

// WhitelistController controller to handle list players authorized on minecraft server
type WhitelistController struct {
	beego.Controller
}

// Whitelist entry to list whitelisted players
// @Title Whitelist
// @Description list whitelisted players
// @Success 200 {object} models.APIResponse
// @router / [get]
func (c *WhitelistController) Whitelist() {
	response, err := rconutils.Command("whitelist", "list")
	if err != nil {
		queryutils.ReplyError(&c.Controller, queryutils.SERVERINTERNALERROR, "issue while sending whitelist command", err)
		return
	}

	respRegExp := regexp.MustCompile("^There are [0-9]+ whitelisted players: (.*)$")
	subMatches := respRegExp.FindSubmatch([]byte(response))

	if len(subMatches) < 2 {
		queryutils.ReplyError(&c.Controller, queryutils.SERVERINTERNALERROR, "issue while parsing whitelist reply", err)
		return
	}

	listRegExp := regexp.MustCompile(`, `)
	players := listRegExp.Split(string(subMatches[1]), -1)

	reply := models.WhitelistResponse{
		Code:    queryutils.OK,
		Players: []string{},
	}

	for _, player := range players {
		reply.Players = append(reply.Players, string(player))
	}

	queryutils.ReplyWithStruct(&c.Controller, queryutils.OK, reply)
}
