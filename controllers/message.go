package controllers

import (
	"encoding/json"

	"github.com/astaxie/beego"
	"gitlab.com/thoratou/minecraft-rcon-service/models"
	"gitlab.com/thoratou/minecraft-rcon-service/queryutils"
	"gitlab.com/thoratou/minecraft-rcon-service/rconutils"
)

// MessageController controller to handle messages to minecraft server console
type MessageController struct {
	beego.Controller
}

// Message entry for messages to sent to player on the minecraft server
// @Title Message
// @Description messages to minecraft server console
// @Success 200 {object} models.APIResponse
// @router / [post]
func (c *MessageController) Message() {
	var messageQuery models.MessageQuery
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &messageQuery); err != nil {
		queryutils.ReplyError(&c.Controller, queryutils.BADREQUEST, "cannot unmarshall message request body", err)
		return
	}

	if err := c.validateQuery(&messageQuery); err != nil {
		queryutils.ReplyError(&c.Controller, queryutils.BADREQUEST, "missing data on message request body", err)
		return
	}

	response, err := rconutils.Command("msg", messageQuery.To, messageQuery.Message)
	if err != nil {
		queryutils.ReplyError(&c.Controller, queryutils.SERVERINTERNALERROR, "issue while sending message command", err)
		return
	}

	queryutils.ReplyOK(&c.Controller, queryutils.OK, response)
}

func (c *MessageController) validateQuery(messageQuery *models.MessageQuery) error {
	if messageQuery.To == "" {
		return queryutils.NewError("missing target player")
	}
	if messageQuery.Message == "" {
		return queryutils.NewError("missing message text")
	}
	return nil
}
