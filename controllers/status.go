package controllers

import (
	"github.com/astaxie/beego"
	"gitlab.com/thoratou/minecraft-rcon-service/models"
)

// StatusController operations about Status
type StatusController struct {
	beego.Controller
}

// Status main controller method
// @Title Status
// @Description status to see if the go server is up
// @Success 200 {object} models.APIResponse
// @router / [get]
func (c *StatusController) Status() {
	status := models.BasicResponse{
		Code:    200,
		Message: "minecraft-rcon-service is up",
	}
	c.Data["json"] = status
	c.ServeJSON()
}
