package controllers

import (
	"encoding/json"

	"github.com/astaxie/beego"
	"gitlab.com/thoratou/minecraft-rcon-service/models"
	"gitlab.com/thoratou/minecraft-rcon-service/queryutils"
	"gitlab.com/thoratou/minecraft-rcon-service/rconutils"
)

// OpController controller to add minecraft players to ops
type OpController struct {
	beego.Controller
}

// Op entry to add minecraft players to ops
// @Title Op
// @Description entry to add minecraft players to ops
// @Success 200 {object} models.APIResponse
// @router / [post]
func (c *OpController) Op() {
	var query models.OpQuery
	if err := json.Unmarshal(c.Ctx.Input.RequestBody, &query); err != nil {
		queryutils.ReplyError(
			&c.Controller,
			queryutils.BADREQUEST,
			"cannot unmarshall op request body", err)
		return
	}

	if err := c.validateQuery(&query); err != nil {
		queryutils.ReplyError(
			&c.Controller,
			queryutils.BADREQUEST,
			"missing data on op request body", err)
		return
	}

	response, err := rconutils.Command("op", query.Player)
	if err != nil {
		queryutils.ReplyError(
			&c.Controller,
			queryutils.SERVERINTERNALERROR,
			"issue while sending op command", err)
		return
	}

	queryutils.ReplyOK(&c.Controller, queryutils.OK, response)
}

func (c *OpController) validateQuery(query *models.OpQuery) error {
	if query.Player == "" {
		return queryutils.NewError("missing player")
	}
	return nil
}
