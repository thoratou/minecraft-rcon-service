package rconutils

import (
	"io"
	"os"
	"strings"

	"github.com/james4k/rcon"
	"gitlab.com/thoratou/minecraft-rcon-service/queryutils"
)

//Command utility function to use minecraft command though rcon protocol
func Command(command ...string) (string, error) {
	host := os.Getenv("RCON_HOST")
	port := os.Getenv("RCON_PORT")
	password := os.Getenv("RCON_PASSWORD")
	remoteConsole, err := rcon.Dial(host+":"+port, password)
	if err != nil {
		return "", queryutils.NewError("Failed to connect to RCON server: " + err.Error())
	}
	defer remoteConsole.Close()

	preparedCmd := strings.Join(command, " ")
	reqID, err := remoteConsole.Write(preparedCmd)

	resp, respID, err := remoteConsole.Read()
	if err != nil {
		if err == io.EOF {
			return "", queryutils.NewError(err.Error())
		}
		return "", queryutils.NewError("Failed to read command:" + err.Error())
	}

	if reqID != respID {
		return "", queryutils.NewError("Weird. This response is for another request.")
	}

	return resp, nil
}
