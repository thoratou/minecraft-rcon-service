package routers

// @APIVersion 1.0.0
// @Title go minecraft admin server
// @Description Light server used inside minecraft docker images
// @Contact thoratou@gmail.com
// @TermsOfServiceUrl
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html

import (
	"strings"

	"gitlab.com/thoratou/cognito-tool/jwtcognito"

	"gitlab.com/thoratou/cognito-tool/jwkscache"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"gitlab.com/thoratou/minecraft-rcon-service/controllers"
)

func init() {

	auth := func(ctx *context.Context) {
		authHeader := ctx.Input.Header("Authorization")
		headerSplit := strings.Split(authHeader, " ")
		if len(headerSplit) != 2 || headerSplit[0] != "Bearer" {
			beego.Error("AuthString invalid:", authHeader)
			ctx.Output.SetStatus(401)
			ctx.Output.Body([]byte("Missing authorization bearer token"))
			return
		}

		jwksCache, err := jwkscache.GetTokenFromGlobalInstance()
		if err != nil {
			beego.Error("JWKS retrieval error:", err.Error())
			ctx.Output.SetStatus(500)
			ctx.Output.Body([]byte("Internal error while retrieving JWKS cache"))
			return
		}

		tokenString := &headerSplit[1]
		beego.Info("Token:", *tokenString)

		_, err = jwtcognito.ParseAndValidateToken(tokenString, jwksCache.Jwks)

		if err != nil {
			beego.Info("Cannot validated token:", err.Error())
			ctx.Output.SetStatus(401)
			ctx.Output.Body([]byte("Invalid token"))
			return
		}
	}

	nsStatus := beego.NewNamespace("/status",
		beego.NSInclude(
			&controllers.StatusController{},
		),
	)

	beego.AddNamespace(nsStatus)

	ns := beego.NewNamespace("/v1",
		beego.NSBefore(auth),
		beego.NSNamespace("/whitelist",
			beego.NSInclude(
				&controllers.WhitelistController{},
			),
			beego.NSNamespace("/add",
				beego.NSInclude(
					&controllers.WhitelistAddController{},
				),
			),
			beego.NSNamespace("/remove",
				beego.NSInclude(
					&controllers.WhitelistRemoveController{},
				),
			),
		),
		beego.NSNamespace("/op",
			beego.NSInclude(
				&controllers.OpController{},
			),
		),
		beego.NSNamespace("/deop",
			beego.NSInclude(
				&controllers.DeopController{},
			),
		),
		beego.NSNamespace("/message",
			beego.NSInclude(
				&controllers.MessageController{},
			),
		),
		beego.NSNamespace("/banner",
			beego.NSInclude(
				&controllers.BannerController{},
			),
		),
	)

	beego.AddNamespace(ns)
}
