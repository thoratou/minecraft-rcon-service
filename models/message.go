package models

//MessageQuery the message query, containing the "to" player and the message to log
type MessageQuery struct {
	To      string `json:"to"`
	Message string `json:"message"`
}
