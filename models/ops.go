package models

//OpQuery the ops add query, containing the player to add to ops
type OpQuery struct {
	Player string `json:"player"`
}

//DeopQuery the ops remove query, containing the player to remove to ops
type DeopQuery struct {
	Player string `json:"player"`
}
