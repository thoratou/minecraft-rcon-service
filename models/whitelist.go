package models

//WhitelistAddQuery the whitelist add query, containing the player to add to whitelist
type WhitelistAddQuery struct {
	Player string `json:"player"`
}

//WhitelistRemoveQuery the whitelist remove query, containing the player to remove to whitelist
type WhitelistRemoveQuery struct {
	Player string `json:"player"`
}

//WhitelistResponse the whitelist reponse, containing the whitelisted player list
type WhitelistResponse struct {
	Code    int      `json:"code"`
	Players []string `json:"players"`
}
