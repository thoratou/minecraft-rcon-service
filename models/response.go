package models

//BasicResponse basic error response structure
type BasicResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

//ErrorResponse common error response structure
type ErrorResponse struct {
	Code          int    `json:"code"`
	Message       string `json:"message"`
	InternalError string `json:"internalError"`
}
