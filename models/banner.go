package models

//BannerQuery the banner query, containing the "to" player
type BannerQuery struct {
	To string `json:"to"`
}

//BannerReply the banner reply, containing the banner metadata
type BannerReply struct {
	To      string  `json:"to"`
	Message string  `json:"message"`
	Banner  *Banner `json:"banner"`
}

//Banner the banner data
type Banner struct {
	Base     string    `json:"base"`
	Patterns []Pattern `json:"patterns"`
}

//Pattern one pattern data
type Pattern struct {
	Pattern string `json:"pattern"`
	Color   int    `json:"color"`
}

//PatternList list there : https://minecraft.gamepedia.com/Banner/Patterns
var PatternList = []string{
	"bs",  //Bottom Stripe
	"ts",  //Top Stripe
	"ls",  //Left Stripe
	"rs",  //Right Stripe
	"cs",  //Center Stripe (Vertical)
	"ms",  //Middle Stripe (Horizontal)
	"drs", //Down Right Stripe
	"dls", //Down Left Stripe
	"ss",  //Small (Vertical) Stripes
	"cr",  //Diagonal Cross
	"sc",  //Square Cross
	"ld",  //Left of Diagonal
	"rud", //Right of upside-down Diagonal
	"lud", //Left of upside-down Diagonal
	"rd",  //Right of Diagonal
	"vh",  //Vertical Half (left)
	"vhr", //Vertical Half (right)
	"hh",  //Horizontal Half (top)
	"hhb", //Horizontal Half (bottom)
	"bl",  //Bottom Left Corner
	"br",  //Bottom Right Corner
	"tl",  //Top Left Corner
	"tr",  //Top Right Corner
	"bt",  //Bottom Triangle
	"tt",  //Top Triangle
	"bts", //Bottom Triangle Sawtooth
	"tts", //Top Triangle Sawtooth
	"mc",  //Middle Circle
	"mr",  //Middle Rhombus
	"bo",  //Border
	"cbo", //Curly Border
	"bri", //Brick
	"gra", //Gradient
	"gru", //Gradient upside-down
	"cre", //Creeper
	"sku", //Skull
	"flo", //Flower
	"moj", //Mojang
	"glb", //Globe
}

//BaseList list there : https://minecraft.gamepedia.com/Banner
var BaseList = []string{
	"white_banner",
	"orange_banner",
	"magenta_banner",
	"light_blue_banner",
	"yellow_banner",
	"lime_banner",
	"pink_banner",
	"gray_banner",
	"light_gray_banner",
	"cyan_banner",
	"purple_banner",
	"blue_banner",
	"brown_banner",
	"green_banner",
	"red_banner",
	"black_banner",
}