package queryutils

type internalError struct {
	what string
}

func (e *internalError) Error() string {
	return e.what
}

//NewError initializer for internal error
func NewError(what string) error {
	err := &internalError{
		what: what,
	}
	return err
}
