package queryutils

import (
	"github.com/astaxie/beego"
	"gitlab.com/thoratou/minecraft-rcon-service/models"
)

//ReplyOK utility for REST reply with message
func ReplyOK(controller *beego.Controller, code int, text string) {
	controller.Ctx.Output.SetStatus(code)
	response := models.BasicResponse{
		Code:    code,
		Message: text,
	}
	controller.Data["json"] = response
	controller.ServeJSON()
}

//ReplyError utility for REST reply with error code and message
func ReplyError(controller *beego.Controller, code int, text string, err error) {
	controller.Ctx.Output.SetStatus(code)
	response := models.ErrorResponse{
		Code:          code,
		Message:       text,
		InternalError: err.Error(),
	}
	controller.Data["json"] = response
	controller.ServeJSON()
}

//ReplyWithStruct utility for REST reply with structured body
func ReplyWithStruct(controller *beego.Controller, code int, body interface{}) {
	controller.Ctx.Output.SetStatus(code)
	controller.Data["json"] = body
	controller.ServeJSON()
}
