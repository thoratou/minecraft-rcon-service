package queryutils

var (
	//OK The request has succeeded.
	OK int = 200
	//BADREQUEST The request could not be understood by the server due to malformed syntax.
	BADREQUEST int = 400
	//SERVERINTERNALERROR The server encountered an unexpected condition which prevented it from fulfilling the request.
	SERVERINTERNALERROR int = 500
)
