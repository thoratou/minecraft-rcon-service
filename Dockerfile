############################
# STEP 1 build executable binary
############################
FROM golang:1.15-alpine as minecraft-rcon-builder

# Add Maintainer Info
LABEL maintainer="Thomas RATOUIT <thoratou@gmail.com>"

# Install git + SSL ca certificates.
# Git is required for fetching the dependencies.
# Ca-certificates is required to call HTTPS endpoints.
RUN apk update && \
    apk add --no-cache git ca-certificates tzdata curl && \
    update-ca-certificates

# Create appuser
RUN addgroup appgroup
RUN adduser -D appuser appgroup

WORKDIR $GOPATH/src/gitlab.com/thoratou/minecraft-rcon-service
COPY . .

#BeeGo
RUN go get -u github.com/astaxie/beego
RUN go get -u github.com/beego/bee

#Cognito tools
RUN go get -u gitlab.com/thoratou/cognito-tool

# Make configuration prod ready
RUN sed -i 's/RunMode = dev/RunMode = prod/g' conf/app.conf

# Fetch dependencies.
RUN go get -d -v

# Build the binary
RUN GOOS=linux go generate
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -a -installsuffix cgo -o /go/bin/server .

############################
# STEP 2 build a small image
############################
FROM scratch

# Add Maintainer Info
LABEL maintainer="Thomas RATOUIT <thoratou@gmail.com>"

# Import from builder.
COPY --from=minecraft-rcon-builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=minecraft-rcon-builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=minecraft-rcon-builder /etc/passwd /etc/passwd
COPY --from=minecraft-rcon-builder /etc/group /etc/group

# Copy our static executable
COPY --chown=appuser:appgroup --from=minecraft-rcon-builder /go/bin/server /go/bin/server

# Copy our bee conf
COPY --chown=appuser:appgroup --from=minecraft-rcon-builder /go/src/gitlab.com/thoratou/minecraft-rcon-service/conf/app.conf /go/bin/conf/app.conf

# Copy SSL certificates
COPY --chown=appuser:appgroup --from=minecraft-rcon-builder /go/src/gitlab.com/thoratou/minecraft-rcon-service/localhost.crt /go/bin/localhost.crt
COPY --chown=appuser:appgroup --from=minecraft-rcon-builder /go/src/gitlab.com/thoratou/minecraft-rcon-service/localhost.key /go/bin/localhost.key

# Use an unprivileged user.
USER appuser

# Expose port 8080 to the outside world
EXPOSE 8080

# Run the hello binary.
WORKDIR /go/bin

ENTRYPOINT ["/go/bin/server"]
