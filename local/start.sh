#!/bin/bash

cd $GOPATH/src/gitlab.com/thoratou/minecraft-rcon-service

go get -u github.com/astaxie/beego
go get -u github.com/beego/bee

go get -d -v

bee run
